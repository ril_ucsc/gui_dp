# Author: Emmanouil "Manolis" Nikolakakis
# Coauthors: Austin Ng, Pakhi Sinha
# Created to be used within the RIL group in UCSC

# Importing necessary packages
import pandas as pd
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy.interpolate import UnivariateSpline
import vaex
import os
import datetime
#from pylab import *
from scipy.optimize import curve_fit
import scipy.optimize as opt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.ticker import NullFormatter  # useful for `logit` scale
import PySimpleGUI as sg
import time
from tkinter import *
import settings
from plotly.subplots import make_subplots
import plotly.graph_objects as go

# Dimensions related functions
def show():
    myLabel = Label(root, text=clicked.get()).pack()
    return myLabel

# Check if user has already defined screen dimensions
def checkDimensions():
    if (settings.screen_res == "UNSET"):
        #Create an instance of tkinter frame
        root = Tk()
        root.title("Adjust screen dimensions")
        root.geometry("400x400")

        clicked = StringVar()
        clicked.set("Select")

        drop = OptionMenu(root, clicked, *settings.options)
        drop.pack()
        root.mainloop()
        dimensions = clicked.get()

        with open("settings.py", "a") as myfile:
            myfile.write("\n")
            myfile.write("screen_res = " + str(settings.option_dict[dimensions]))
        return settings.option_dict[dimensions]

#  The magic function that makes it possible.... glues together tkinter and pyplot using Canvas Widget
def draw_figure(canvas, figure):
    if not hasattr(draw_figure, 'canvas_packed'):
        draw_figure.canvas_packed = {}
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    widget = figure_canvas_agg.get_tk_widget()
    if widget not in draw_figure.canvas_packed:
        draw_figure.canvas_packed[widget] = figure
        widget.pack(side='top', fill='both', expand=1)
    return figure_canvas_agg

def delete_figure_agg(figure_agg):
    figure_agg.get_tk_widget().forget()
    try:
        draw_figure.canvas_packed.pop(figure_agg.get_tk_widget())
    except Exception as e:
        print(f'Error removing {figure_agg} from list', e)
    plt.close('all')

def input_data(file_name):
    # Reading the Data file: Notice the delimiter <SPACE>
    data = pd.read_csv(file_name, delimiter=' ')

    # Creating Csv files
    file_path = 'final_data.csv'
    data.to_csv(file_path, index=False)

    #Create Hdf5 files
    vaex_data = vaex.from_csv(file_path, convert=True, chunk_size=5_000_000)
    vaex_df = vaex.open('final_data.csv.hdf5')
    return vaex_df

def export_images(vaex_data):
    ct = str(datetime.datetime.now())
    ct=str(ct).replace(" ","_")
    root_output="output_" + str(ct)
    os.mkdir(root_output)
    for node in range(0,5):
        print("Exporting node "+str(node)+" ...")
        os.mkdir(root_output + "/node_" + str(node))
        for board in range(27,31):
            os.mkdir(root_output + "/node_" + str(node) +"/board_" + str(board))
            for rena in range(0,2):
                os.mkdir(root_output + "/node_" + str(node) +"/board_" + str(board) + "/rena_" + str(rena))
                for channel in range(0,31):
                    filtered_adc_e = []   
                    filtered_adc_e = vaex_data.filter((vaex_data.node == int(node)))
                    filtered_adc_e = filtered_adc_e.filter((filtered_adc_e.board == int(board)))
                    filtered_adc_e = filtered_adc_e.filter((filtered_adc_e.rena == int(rena)))
                    filtered_adc_e = filtered_adc_e.filter((filtered_adc_e.channel == int(channel)))
                    pandas_filtered = filtered_adc_e.to_pandas_df()
                    plt.figure()
                    plt.hist(pandas_filtered["adc_e"], bins=1000, edgecolor='black')
                    plt.savefig(root_output + "/node_" + str(node) +"/board_" + str(board) + "/rena_" + str(rena) + "/" + str(channel) + ".jpg") #save as jpg
                    plt.clf()
                    plt.close()
    return

def read_text_file(file_path):
##    with open(file_path, 'r') as f:
##        return(f.read())
    try:
        with open(file_path, 'r') as f:
            return(f.read())
    except Exception as e:
        print("This file does not exist!")


def get_ch_data(vaex_data, node, board, rena, channel):
    filtered_adc_e = []
    print(vaex_data.head())
    filtered_adc_e = vaex_data.filter((vaex_data.node == int(node)))
    filtered_adc_e = filtered_adc_e.filter((filtered_adc_e.board == int(board)))
    filtered_adc_e = filtered_adc_e.filter((filtered_adc_e.rena == int(rena)))
    filtered_adc_e = filtered_adc_e.filter((filtered_adc_e.channel == int(channel)))

    pandas_filtered = filtered_adc_e.to_pandas_df()
    try:
        print (type(pandas_filtered["adc_e"])) # panda.core.series.Series
        return pandas_filtered["adc_e"]
    except Exception as e:
        print("This variable does not exist")


def plot_channel(vaex_data, node, board, rena, channel, adapt_multiplier):
    t0_filt = time.perf_counter()                   # start time to filter all data

    filtered_adc_e = []
    #filtered_adc_e = vaex_data.filter((vaex_data.node == int(node)))
    filtered_adc_e = vaex_data.filter((vaex_data.board == int(board)))
    filtered_adc_e = filtered_adc_e.filter((filtered_adc_e.rena == int(rena)))
    filtered_adc_e_1 = filtered_adc_e.filter((filtered_adc_e.channel == int(channel)))
    filtered_adc_e_2 = filtered_adc_e.filter((filtered_adc_e.channel == int(channel)+1))
    filtered_adc_e_3 = filtered_adc_e.filter((filtered_adc_e.channel == int(channel)+2))
    filtered_adc_e_4 = filtered_adc_e.filter((filtered_adc_e.channel == int(channel)+3))

    # 5 more graphs
    filtered_adc_e_5 = filtered_adc_e.filter((filtered_adc_e.channel == int(channel)+4))
    filtered_adc_e_6 = filtered_adc_e.filter((filtered_adc_e.channel == int(channel)+5))
    filtered_adc_e_7 = filtered_adc_e.filter((filtered_adc_e.channel == int(channel)+6))
    filtered_adc_e_8 = filtered_adc_e.filter((filtered_adc_e.channel == int(channel)+7))
    filtered_adc_e_9 = filtered_adc_e.filter((filtered_adc_e.channel == int(channel)+8))


    pandas_filtered_1 = filtered_adc_e_1.to_pandas_df()
    pandas_filtered_2 = filtered_adc_e_2.to_pandas_df()
    pandas_filtered_3 = filtered_adc_e_3.to_pandas_df()
    pandas_filtered_4 = filtered_adc_e_4.to_pandas_df()


    # 5 more graphs
    pandas_filtered_5 = filtered_adc_e_5.to_pandas_df()
    pandas_filtered_6 = filtered_adc_e_6.to_pandas_df()
    pandas_filtered_7 = filtered_adc_e_7.to_pandas_df()
    pandas_filtered_8 = filtered_adc_e_8.to_pandas_df()
    pandas_filtered_9 = filtered_adc_e_9.to_pandas_df()


    t1_filt = time.perf_counter()                   # end time to filter all data
    print("Total filter timing: ", t1_filt - t0_filt)     # time it takes to filter

    #sorted_adc = pandas_filtered["adc_e"].tolist()
    #sorted_adc.sort()
    #counts_adc = {}
    #print(sorted_adc)
    #current_element = sorted_adc[0]
    #counts_adc[str(current_element)] = sorted_adc.count(current_element)
    #elem = []
    #c_adc = []
    #for i in range(len(sorted_adc)):
    #    if sorted_adc[i] != current_element:
    #        current_element = sorted_adc[i]
    #        counts_adc[str(current_element)] = sorted_adc.count(current_element)
    #        elem.append(current_element)
    #        c_adc.append(counts_adc[str(current_element)])
    #elem = np.array(elem)
    #c_adc = np.array(c_adc)
    #plt.hist(pandas_filtered["adc_e"], bins=1000, edgecolor='black')




    '''
    # Plotly 4 Graphs
    fig = make_subplots(subplot_titles=['Energy Histogram: Channel '+str(channel), 
                                        'Energy Histogram: Channel '+str(channel+1), 
                                        'Energy Histogram: Channel '+str(channel+2), 
                                        'Energy Histogram: Channel '+str(channel+3)],
                                        horizontal_spacing=None, vertical_spacing=0.12,
                                        rows=2, cols=2)

    t0_plotly = time.perf_counter()
    fig.append_trace(go.Histogram(x=pandas_filtered_1["adc_e"], nbinsx=1000, showlegend=False, marker_color='black'), row=1, col=1)
    t1_plotly = time.perf_counter()                                
    print("Plotly single graph time: ", t1_plotly - t0_plotly)

    fig.append_trace(go.Histogram(x=pandas_filtered_2["adc_e"], nbinsx=1000, showlegend=False, marker_color='black'), row=1, col=2)
    fig.append_trace(go.Histogram(x=pandas_filtered_3["adc_e"], nbinsx=1000, showlegend=False, marker_color='black'), row=2, col=1)
    fig.append_trace(go.Histogram(x=pandas_filtered_4["adc_e"], nbinsx=1000, showlegend=False, marker_color='black'), row=2, col=2)
    
    fig.update_layout(height=700, width=1000,font=dict(size=18*adapt_multiplier))
    fig.update_annotations(font_size=19*adapt_multiplier)

    t4_plotly = time.perf_counter()                                
    print("Plotly 4 graphs time: ", t4_plotly - t0_plotly)

    fig.write_image("graph.png")
    # fig.show()
    return "graph.png"
    '''
    

    # Plotly 9 Graphs
    fig = make_subplots(subplot_titles=['Energy Histogram: Channel '+str(channel), 
                                        'Energy Histogram: Channel '+str(channel+1), 
                                        'Energy Histogram: Channel '+str(channel+2), 
                                        'Energy Histogram: Channel '+str(channel+3),
                                        'Energy Histogram: Channel '+str(channel+4),
                                        'Energy Histogram: Channel '+str(channel+5),
                                        'Energy Histogram: Channel '+str(channel+6),
                                        'Energy Histogram: Channel '+str(channel+7),
                                        'Energy Histogram: Channel '+str(channel+8)],
                                        horizontal_spacing=None, vertical_spacing=0.12,
                                        rows=3, cols=3)

    t0_plotly = time.perf_counter()
    fig.append_trace(go.Histogram(x=pandas_filtered_1["adc_e"], nbinsx=1000, showlegend=False, marker_color='black'), row=1, col=1)
    t1_plotly = time.perf_counter()                                
    print("Plotly single graph time: ", t1_plotly - t0_plotly)

    fig.append_trace(go.Histogram(x=pandas_filtered_2["adc_e"], nbinsx=1000, showlegend=False, marker_color='black'), row=1, col=2)
    fig.append_trace(go.Histogram(x=pandas_filtered_3["adc_e"], nbinsx=1000, showlegend=False, marker_color='black'), row=1, col=3)
    fig.append_trace(go.Histogram(x=pandas_filtered_4["adc_e"], nbinsx=1000, showlegend=False, marker_color='black'), row=2, col=1)
    fig.append_trace(go.Histogram(x=pandas_filtered_5["adc_e"], nbinsx=1000, showlegend=False, marker_color='black'), row=2, col=2)
    fig.append_trace(go.Histogram(x=pandas_filtered_6["adc_e"], nbinsx=1000, showlegend=False, marker_color='black'), row=2, col=3)
    fig.append_trace(go.Histogram(x=pandas_filtered_7["adc_e"], nbinsx=1000, showlegend=False, marker_color='black'), row=3, col=1)
    fig.append_trace(go.Histogram(x=pandas_filtered_8["adc_e"], nbinsx=1000, showlegend=False, marker_color='black'), row=3, col=2)
    fig.append_trace(go.Histogram(x=pandas_filtered_9["adc_e"], nbinsx=1000, showlegend=False, marker_color='black'), row=3, col=3)

    fig.update_layout(height=700, width=1000,font=dict(size=18*adapt_multiplier))
    fig.update_annotations(font_size=19*adapt_multiplier)

    t9_plotly = time.perf_counter()                                
    print("Plotly 9 graphs time: ", t9_plotly - t0_plotly)

    fig.write_image("fig_graph.png")
    # fig.show()
    return "fig_graph.png"


    
    '''
    # USING MATPLOTLIB

    # plot with various axes scales
    plt.figure(figsize=(10*adapt_multiplier,8*adapt_multiplier))
    
    # Channel
    plt.subplot(221)
    t0 = time.perf_counter()                                # start time to plot
    plt.hist(pandas_filtered_1["adc_e"], bins=1000, edgecolor='black')
    t1 = time.perf_counter()                                # end time to plot 1 graph
    print("Matplotlib single graph time: ", t1 - t0)          # time to plot 1 graph

    #plt.yscale('ADC Value')
    plt.xticks(fontsize=7*adapt_multiplier)
    plt.yticks(fontsize=7*adapt_multiplier)
    plt.title('Energy Histogram: Channel '+str(channel), fontsize=7*adapt_multiplier)
    plt.grid(True)
    
    # Channel + 1
    plt.subplot(222)
    plt.hist(pandas_filtered_2["adc_e"], bins=1000, edgecolor='black')
    #plt.yscale('ADC Value')
    plt.xticks(fontsize=7*adapt_multiplier)
    plt.yticks(fontsize=7*adapt_multiplier)
    plt.title('Energy Histogram: Channel '+str(channel+1), fontsize=7*adapt_multiplier)
    plt.grid(True)

    # Channel + 2
    plt.subplot(223)
    plt.hist(pandas_filtered_3["adc_e"], bins=1000, edgecolor='black')
    #plt.yscale('ADC Value', linthreshy=0.01)
    plt.xticks(fontsize=7*adapt_multiplier)
    plt.yticks(fontsize=7*adapt_multiplier)
    plt.title('Energy Histogram: Channel '+str(channel+2), fontsize=7*adapt_multiplier)
    plt.grid(True)

    # Channel + 3
    plt.subplot(224)
    plt.hist(pandas_filtered_4["adc_e"], bins=1000, edgecolor='black')
    t4 = time.perf_counter()                                # end time to plot 4 graphs
    print("Matplotlib 4 graphs time: ", t4 - t0)              # time to plot 4 graphs
    #plt.yscale('ADC Value')
    plt.xticks(fontsize=7*adapt_multiplier)
    plt.yticks(fontsize=7*adapt_multiplier)
    plt.title('Energy Histogram: Channel '+str(channel+3), fontsize=7*adapt_multiplier)
    plt.grid(True)
    

    # Format the minor tick labels of the y-axis into empty strings with
    # `NullFormatter`, to avoid cumbering the axis with too many labels.
    plt.gca().yaxis.set_minor_formatter(NullFormatter())
    # Adjust the subplot layout, because the logit one may take more space
    # than usual, due to y-tick labels like "1 - 10^{-3}"
    plt.subplots_adjust(top=0.92, bottom=0.08, left=0.07, right=0.95, hspace=0.25,
                        wspace=0.35)
    # return plt.gcf()
    #return plt.show()
    '''
    
    
    return
    
