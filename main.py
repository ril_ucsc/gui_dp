# Author: Emmanouil "Manolis" Nikolakakis
# Coauthors: Austin Ng, Pakhi Sinha
# Created to be used within the RIL group in UCSC

import PySimpleGUI as sg
import functions
import vaex
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import yaml
from PIL import Image, ImageTk
from io import BytesIO
import time
import os
# from tkinter import *
import settings

# Check if user has already defined screen dimensions
resolution = functions.checkDimensions()

# Obtain screen dimensions then gets the adaption multiplier
random_window = sg.Window("tkinter made me open a new app to get the screen dimensions")
screen_dimensions = random_window.get_screen_size()

if (resolution != None):
    adapt_multiplier = (screen_dimensions[0] * resolution)
else:
    adapt_multiplier = (screen_dimensions[0] * settings.screen_res)

# Tracks if there's a graph present in the window
graph_present = False
param   = (20,3) # size of the main window

sg.theme(settings.theme)
sg.set_options(font=(settings.font, int(12 * adapt_multiplier)))

listing = [sg.Text(u, size = param) for u in settings.columns]
listing2 = [sg.Text(u, size = param) for u in settings.columns2]

core1 = [
    sg.Input(size=param, enable_events=True, key='NODE_1'),
    sg.Input(size=param, enable_events=True, key='BOARD_1'),
    sg.Input(size=param, enable_events=True, key='RENA_1'),
]

core2 = [
    sg.Input(size=param, enable_events=True, key='Rena'),
    sg.Input(size=param, enable_events=True, key='Channel'),
    sg.Input(size=param, enable_events=True, key='FastDAC'),
    sg.Input(size=param, enable_events=True, key='SlowDAC')
    
]

mesh = [[x,y] for (x,y) in list(zip(listing, core1))]
mesh = [[x,y] for (x,y) in list(zip(listing2, core2))]

###Building Window

_VARS = {'window': False,
         'fig_agg': False,
         'pltFig': False,
         'dataSize': 60}

AppFont = 'Any 16'
SliderFont = 'Any 14'
node=3
board=29
rena=0
rena_update_yaml=0
channel=5
channel_update_yaml=0
fastdac=0
slowdac=0
coin_file=""
yaml_file=""
yaml_data=[]
stored_channels=[]
tp_data=[]
fig = 0

figure_w, figure_h = 550, 550 # this kind of seems like the...text box?
figure_agg = None

# Adapting banner size starts here
# Takes your file, resizes it, and returns a new resized image file
def image_resizer(filename, img_w, img_h):
    img = Image.open(filename)
    img = img.resize((int(img_w * adapt_multiplier), int(img_h * adapt_multiplier)), resample = Image.ANTIALIAS)
    new_img = BytesIO()
    img.save(new_img, format="png")
    new_img.name = 'adapted_img.png'
    new_img.seek(0)
    return new_img

# Reads the image file
image_data = image_resizer('logo.png', 960, 124).read()

layout = [[sg.Image(data = image_data, key='-IMAGE-')], [sg.Button(settings.plot_channels),
         sg.Text(text="Scroll through channels:",
                   font=SliderFont,
                   background_color=settings.bkg_color,
                   pad=((0, 0), (10, 0)),
                   text_color='Black'),
         sg.Slider(range=(1, 32), orientation='h', size=(34, 20),
                     default_value=_VARS['dataSize'],
                     background_color=settings.bkg_color,
                     text_color='Black',
                     key='-Slider-',
                     enable_events=True),], \
         core1, listing, [sg.Button(settings.updata_daq)], core2, listing2, [sg.Text(settings.choose_file)], [sg.FileBrowse(key="-IN-"),sg.Button(settings.submit)], [sg.Text(settings.choose_file_yaml)], [sg.FileBrowse(key="-IN_YAML-"),sg.Button(settings.submit_yaml)], [sg.Button(settings.export)], \
                    [],[sg.Image(key='-GRAPH-')]]

# Create the window
window = sg.Window("DAQ ADC Plotting", layout, size=(1200,1000), finalize=True)
slider = window['-Slider-']

# Checks if you release the mouse
# binds the action of releasing the slider to the print statement "-Slider- Release"
slider.bind('<ButtonRelease-1>', " Release")

# Create an event loop
while True:
    event, values = window.read()
    # End program if user closes window or
    # presses the OK button
    #if fig:
    #    try:                                  # call function to get the figure
    #        figure_agg = draw_figure(window['-CANVAS-'].TKCanvas, fig)  # draw the figure
    #    except Exception as e:
    #        print('Exception in function', e)
    if event == settings.plot_channels:
        #if figure_agg:
        # ** IMPORTANT ** Clean up previous drawing before drawing again
        #    functions.delete_figure_agg(figure_agg)

        if graph_present == True:
            print("Graph closed.")
            window["-GRAPH-"].update(data = '')
            graph_present = False
        
        else:
            t0_plot = time.perf_counter()                       # tracks the initial time for plotting
            fig = functions.plot_channel(vaex_data, node, board, rena, channel, adapt_multiplier)
            adapt_fig_data = image_resizer(fig, 725, 550).read()          # resizes the graph
            window["-GRAPH-"].update(data = adapt_fig_data)

            graph_present = True

            t1_plot = time.perf_counter()                       # final time for plotting
            print("Total Loading Time: ", t1_plot - t0_plot)    # total time to plot
            print("________next channel_________")


        '''
        figure_agg = functions.draw_figure(window['-CANVAS-'].TKCanvas, fig)
        t2_draw_plot = time.perf_counter()                   # final time for plotting
        print("Total Draw Time: ", t2_draw_plot - t0_plot)
        '''

    
    elif event == "NODE_1":
        node = values[event]
    elif event == "BOARD_1":
        board = values[event]
    elif event == "RENA_1":
        rena = values[event]
    elif event == "Channel":
        channel_update_yaml = values[event]
    elif event == "Rena":
        rena_update_yaml = values[event]
    elif event == "FastDAC":
        fastdac = values[event]
    elif event == "SlowDAC":
        slowdac = values[event]
    elif event == "Export All":
        functions.export_images(vaex_data)
    elif event == "Submit":
        print(values["-IN-"])
        print("Loading...")
        coin_file = values["-IN-"]
        vaex_data = functions.input_data(coin_file)
        print("Loading Completed!")
    elif event == "Submit Yaml":
        print(values["-IN_YAML-"])
        yaml_in = values["-IN_YAML-"]
        with open(yaml_in) as f:
            yaml_data = yaml.load(f, Loader=yaml.FullLoader)
    elif event == settings.updata_daq:
        print("Previous FastDAC:", str(yaml_data["Rena["+str(rena_update_yaml)+"]"]["Channel["+str(channel_update_yaml)+"]"]["FastDac"]))
        print("New FastDAC: " + str(fastdac))
        print("Previous SlowDAC:    ", str(yaml_data["Rena["+str(rena_update_yaml)+"]"]["Channel["+str(channel_update_yaml)+"]"]["SlowDac"]))
        print("New SlowDAC: " + str(slowdac))
        yaml_out = yaml_in.replace(".yml","") + "_updated.yml"
        yaml_data["Rena["+str(rena_update_yaml)+"]"]["Channel["+str(channel_update_yaml)+"]"]["FastDac"]=int(fastdac)
        yaml_data["Rena["+str(rena_update_yaml)+"]"]["Channel["+str(channel_update_yaml)+"]"]["SlowDac"]=int(slowdac)
        with open(yaml_out, 'w') as f: #'w' stands for writing mode
            yaml.dump(yaml_data, f, sort_keys=False)
    elif event == '-Slider-':
        channel = int(values['-Slider-'])

        '''
        Too slow!
        Was updating the graph as the slider was moving

        if event in ("-Slider- Release"):
        # if graph_present == True:
            t0_plot = time.perf_counter()                       # tracks the initial time for plotting
            
            fig = functions.plot_channel(vaex_data, node, board, rena, channel)
            adapt_fig_data = image_resizer(fig, 725, 550).read()          # resizes the graph
            window["-GRAPH-"].update(data = adapt_fig_data)

            t1_plot = time.perf_counter()                       # final time for plotting
            print("Total Loading Time: ", t1_plot - t0_plot)    # total time to plot
            print("________next channel_________")

        #fig = functions.plot_channel(vaex_data, node, board, rena, channel)
        #if fig:
        #    figure_agg = functions.draw_figure(window['-CANVAS-'].TKCanvas, fig)
        # print(int(values['-Slider-']))
        '''
    # Makes sure the slider is released and that a graph is present before graphing
    # Small Issue: You have to print out 
    elif event in ("-Slider- Release") and graph_present == True:
        t0_plot = time.perf_counter()                       # tracks the initial time for plotting
            
        fig = functions.plot_channel(vaex_data, node, board, rena, channel, adapt_multiplier)
        adapt_fig_data = image_resizer(fig, 725, 550).read()          # resizes the graph
        window["-GRAPH-"].update(data = adapt_fig_data)

        t1_plot = time.perf_counter()                       # final time for plotting
        print("Total Loading Time: ", t1_plot - t0_plot)    # total time to plot
        print("________next channel_________")
    elif event == sg.WIN_CLOSED:
        break

window.close()
