# GUI Text
columns = ["NODE","BOARD","RENA"]
columns2 = ["Rena","Channel", "FastDAC", "SlowDAC"]
plot_channels="Plot Channels         "
updata_daq="Update DAC for channel"
choose_file="Choose a data file: "
submit="Submit"
export="Export All"
choose_file_yaml="Choose a configuration file: "
submit_yaml="Submit Yaml"
bkg_color='#FDF6E3'

# Others
theme = 'Dark Brown 1'
font = 'Courier New'
bkg_color = '#FDF6E3'
screen_res = 0.75/1200
options = [
    "1280 x 720",
    "1366 x 768",
    "1440 x 900",
    "1536 x 864",
    "1920 x 1080"
    ]
option_dict = {"1280 x 720": 0.75/4000, "1366 x 768": 0.75/3500,"1440 x 900":0.75/3000, "1536 x 864":0.75/2000,"1920 x 1080":0.75/1200}
screen_res = "UNSET"


