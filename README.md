Main Developer: Emmanouil "Manolis" Nikolakakis

Co-developers: Austin Ng, Pakhi Sinha

This software has been written to plot data from the PET system and update configuration files (DAC levels) for RIL.

First, make sure to go through the documentation.pdf

To setup the python package requirements execute: pip install -r requirements.txt

To start the GUI go to the directory of the software and type: python main.py (the first time you will be prompted to select your screen dimensions)

For any bug issues or other requirements:

Primary contacts:

Austin Ng: audng@ucsc.edu

Pakhi Sinha: pasinha@ucsc.edu

Secondary contact:

Manolis Nikolakakis: enikolak@ucsc.edu
